public class BikeStore{
    public static void main(String[] args){
        Bicycle[] bikes=new Bicycle[4];
        bikes[0]=new Bicycle("Bob", 5, 68.5);
        bikes[1]=new Bicycle("Greg",6,40.1);
        bikes[2]=new Bicycle("Mark",8,45.5);
        bikes[3]=new Bicycle("Sam",7,56.8);
        for (Bicycle bike : bikes){
            System.out.println(bike);
        }
    } 
}